<?php

function solve($imageWidth, $imageHeight, array $image, $patternWidth, $patternHeight, array $pattern) {
    // Write your code here
    // To debug (equivalent to var_dump): error_log(var_export($var, true));
    for($i = 0; $i < $imageHeight; $i++)
    {
      for($j = 0; $j < $imageWidth; $j++)
      {
          $valid = true;
          $compteur = 0;
          for($n = 0; $n < $patternHeight; $n++)
          {
            for($m = 0; $m < $imageWidth; $m++)
            {
              $chaine = $image[$i + $n];
              $char1 = substr($chaine, $j + $m, 1);
  
              $chaine2 = $pattern[$n];
              $char2 = substr($chaine2, $m, 1);
  
              if($char1 != $char2)
                  $valid = false;
  
              if($valid == false)
                  break;
  
              $compteur++;
              if($compteur == $imageHeight*$imageWidth)
                  return [$i,$j];
            }
  
            if($valid == false)
              break;
          } 
      }
    }
  }

  
function computeCheckDigit($identificationNumber) {
    $int = intval($identificationNumber);
    $val2 = 0;
    for($i = 0; strlen($int); $i++){
      if($i % 2 == 0){
        $val = $int / (pow(10, $i));
        
        $val2 = $val2 + $val;
      }
      
    }
    return $val2;
  }
